# DSA-2000 RCF Design

## Get the Document!

You can download the latest gitlab CI-built document [here](https://gitlab.com/api/v4/projects/56928121/jobs/artifacts/main/raw/D2k-00016-RCF-DES-Preliminary_Design.pdf?job=build_job)

## Build Requirements

Documentation is built using pandoc and requires the following packages:

- pandoc (tested with 3.2)
- pandoc-citeproc
- latex

On `Ubuntu 22.04` these can be installed with:

``` shell
apt install texlive-latex-recommended texlive-latex-extra pandoc pandoc-citeproc
```

## Instructions for generating PDF version of documentation

To build the document locally:

### Print version

``` shell
./build_local_pdf.sh
```
