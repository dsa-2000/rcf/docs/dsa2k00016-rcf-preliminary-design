---
title: RCF Preliminary Design
author:
- Jack Hickish\textsuperscript{1}
affiliation:
- \textsuperscript{1} Real-Time Radio Systems Ltd
abstract:
document-number: 00016
wbs-level-2-abbrev: RCF
document-type-abbrev: DES
revisions:
- version: 1
  date: 2023-10-02
  remarks: Original
  authors:
  - JH
- version: 2
  date: 2023-10-17
  sections:
  - sec:Requirements
  remarks: Re-designate as design document. Minor typo/formatting fixes. Add Beamforming requirement.
  authors:
  - JH
- version: 2.1
  date: 2023-10-17
  sections:
  - sec:Design
  remarks: Typo fixes. Cross-ref fixes. Table overflow fixes.
  authors:
  - JH
- version: 3
  date: 2023-10-19
  remarks: Changes after internal review
  authors:
  - JH
- version: 3.1
  date: 2023-10-20
  sections:
  - sec:Glossary
  remarks: Expand gloassary
  authors:
  - JH
- version: 3.1.1
  date: 2023-10-20
  remarks: Minor grammar / formatting fixes
  authors:
  - JH
- version: 4
  date: 2023-10-20
  sections:
  - sec:Introduction
  remarks: Add introduction
  authors:
  - JH
- version: 4.0.1
  date: 2024-05-14
  remarks: Change template. No content changes.
  authors:
  - JH
...
